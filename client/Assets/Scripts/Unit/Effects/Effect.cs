using UnityEngine;

namespace ProjectSP.Effect
{
	public class Effect : MonoBehaviour
	{
		private EffectManager currentManager;

		public void SetEffectManager(EffectManager manager)
		{
			currentManager = manager;
		}

		private void OnDestroy()
		{
			if (currentManager == null) return;
			currentManager.Remove(this);
		}
	}
}