using UnityEngine;

namespace ProjectSP
{
	public class PlayerInputController : MonoBehaviour
	{
		[SerializeField] private BaseVirtualJoystick joystick;
		[SerializeField] private Vector3Variable moveVector;

		private void Update()
		{
			var movingVec = GetVecFromJoystick(joystick, Vector3.forward, Vector3.right);
			movingVec.Normalize();
			moveVector.Value = movingVec;
		}

		private Vector3 GetVecFromJoystick(BaseVirtualJoystick joystick, Vector3 fwd, Vector3 right)
		{
			float horizontalInput = joystick.Horizontal;
			float verticalInput = joystick.Vertical;
			Vector3 res = verticalInput * fwd + horizontalInput * right;
			return res;
		}
	}
}