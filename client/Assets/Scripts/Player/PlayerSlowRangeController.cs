using System.Collections.Generic;
using UnityEngine;

namespace ProjectSP
{
	public class PlayerSlowRangeController : MonoBehaviour
	{
		[SerializeField] private EnemiesSet targetEnemies;

		private List<EnemyController> enemiesUnderEffect = new List<EnemyController>();

		private void Update()
		{
			//TODO: если есть противники замедляем игрока
			for (int i = enemiesUnderEffect.Count - 1; i >= 0; i--)
			{
				if (targetEnemies.Contains(enemiesUnderEffect[i])) continue;
				enemiesUnderEffect.RemoveAt(i);
			}
		}

		private void ActiveteEffect()
		{
		}
	}
}