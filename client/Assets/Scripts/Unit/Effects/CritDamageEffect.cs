using UnityEngine;

namespace ProjectSP.Effect
{
	public class CritDamageEffect : Effect, IDamageModifier
	{
		[SerializeField] private float critModifier = 2.5f;
		[Range(0, 1)] [SerializeField] private float critChance = .15f;
		[SerializeField] private GameObject visual;
		[SerializeField] private float cooldownTime;

		private float lastProcTime;

		private PseudoRandomBoolean randomSeed;

		private void Awake()
		{
			randomSeed = new PseudoRandomBoolean(critChance);
			visual.SetActive(false);
			lastProcTime = -cooldownTime;
		}

		public float Multiplier()
		{
			if (lastProcTime + cooldownTime > Time.timeSinceLevelLoad) return 1;
			var crit = randomSeed;
			if (crit)
			{
				visual.SetActive(false);
				visual.SetActive(true);
				lastProcTime = Time.timeSinceLevelLoad;
			}

			return randomSeed ? critModifier : 1;
		}
	}
}