using UnityEngine;
using UnityEngine.EventSystems;

namespace ProjectSP
{
	public class FloatVirtualJoystick : FixedVirtualJoystick
	{
		[SerializeField] private RectTransform joystickArea;

		[Header("Borders")] [SerializeField] private float leftBorderOffset = 0;
		[SerializeField] private float rightBorderOffset = 0;
		[SerializeField] private float bottomBorderOffset = 0;
		[SerializeField] private float topBorderOffset = 0;

		private Vector2 initialAnchoredPos;
		private Vector2 initialAnchorMin;
		private Vector2 initiaAnchorMax;

		protected override void Awake()
		{
			initialAnchorMin = background.anchorMin;
			initiaAnchorMax = background.anchorMax;
			initialAnchoredPos = background.anchoredPosition;

			base.Awake();
		}

		public override void OnPointerDown(PointerEventData eventData)
		{
			RectTransformUtility.ScreenPointToLocalPointInRectangle(joystickArea, eventData.position, uiCamera,
				out var inpPos);

			Vector2 halfVec = Vector2.one * 0.5f;
			background.anchorMin = halfVec;
			background.anchorMax = halfVec;

			background.anchoredPosition = inpPos;
			base.OnPointerDown(eventData);
		}

		public override void OnPointerUp(PointerEventData eventData)
		{
			background.anchorMin = initialAnchorMin;
			background.anchorMax = initiaAnchorMax;
			background.anchoredPosition = initialAnchoredPos;
			base.OnPointerUp(eventData);
		}
	}
}