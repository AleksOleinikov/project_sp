using UnityEngine;

namespace ProjectSP
{
	public class PlayerDamageZoneScaler : MonoBehaviour
	{
		[SerializeField] private GameObject damageZone;
		[SerializeField] private PlayerConfig playerConfig;

		private void Awake()
		{
			SetZoneScale();
		}

		private void SetZoneScale()
		{
			damageZone.transform.localScale = new Vector3(playerConfig.weapon.attackRange.Value,
				playerConfig.weapon.attackRange.Value, playerConfig.weapon.attackRange.Value);
		}
	}
}