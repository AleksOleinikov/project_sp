using ProjectSP.Events;
using UnityEditor;
using UnityEngine;

namespace ProjectSP.Editor
{
	[CustomEditor(typeof(BaseGameEvent<>), true)]
	public class BaseGameEventEditor : UnityEditor.Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			GUI.enabled = Application.isPlaying;

			var e = (IGameEvent) target;
			if (GUILayout.Button("Raise"))
			{
				e.Raise();
			}
		}
	}
}