using UnityEngine;

namespace ProjectSP.Effect
{
	public class PlayerWeaponEffectsActivator : MonoBehaviour
	{
		[SerializeField] private PlayerConfig config;
		[SerializeField] private EffectManager effectManager;

		private void Awake()
		{
			ActivateEffects();
		}

		private void ActivateEffects()
		{
			foreach (var effect in config.weapon.effects)
			{
				effectManager.CreateEffect(effect);
			}
		}
	}
}