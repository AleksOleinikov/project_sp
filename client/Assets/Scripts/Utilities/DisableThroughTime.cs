using System.Collections;
using UnityEngine;

namespace ProjectSP
{
	public class DisableThroughTime : MonoBehaviour
	{
		[SerializeField] private float timeTDisable;

		private WaitForSeconds wait;

		private void Awake()
		{
			wait = new WaitForSeconds(timeTDisable);
		}

		private void OnEnable()
		{
			StartCoroutine(Disable());
		}

		IEnumerator Disable()
		{
			yield return wait;
			gameObject.SetActive(false);
		}
	}
}