using UnityEngine;

namespace ProjectSP.Events
{
	[CreateAssetMenu(menuName = "Events/VoidGameEvent")]
	public class VoidGameEvent : BaseGameEvent<Void>
	{
		public override void Raise() => Raise(new Void());
	}
}