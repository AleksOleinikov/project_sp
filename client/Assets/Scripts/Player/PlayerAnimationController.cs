using UnityEngine;

namespace ProjectSP
{
	public class PlayerAnimationController : MonoBehaviour
	{
		[SerializeField] private Animator animator;

		private const string BATTLE_START_TRIGGER = "OnBattleStart";
		private const string BATTLE_END_TRIGGER = "OnBattleEnd";

		public void OnBattleStart()
		{
			animator.SetTrigger(BATTLE_START_TRIGGER);
		}

		public void OnBattleEnd()
		{
			animator.SetTrigger(BATTLE_END_TRIGGER);
		}
	}
}