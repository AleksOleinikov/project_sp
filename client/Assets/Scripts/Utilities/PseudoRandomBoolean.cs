namespace ProjectSP
{
	public class PseudoRandomBoolean
	{
		float _baseProbability;
		float _currentProbability;
		int iteration;

		public float baseProbability
		{
			get { return _baseProbability; }
			set
			{
				_baseProbability = value;
				Reset();
			}
		}


		public PseudoRandomBoolean()
		{
		}

		public PseudoRandomBoolean(float probability)
		{
			baseProbability = probability;
		}


		public static implicit operator bool(PseudoRandomBoolean b)
		{
			if (UnityEngine.Random.Range(0f, 1f) < b._currentProbability)
			{
				b.Reset();
				return true;
			}
			else
			{
				b._currentProbability += (1f - b._currentProbability) * b._baseProbability;
				b.iteration++;
				return false;
			}
		}

		public float GetCurrentProbability()
		{
			return _currentProbability;
		}

		public void Reset()
		{
			_currentProbability = _baseProbability;
			iteration = 0;
		}

		public int GetIterationCount()
		{
			return iteration;
		}
	}
}