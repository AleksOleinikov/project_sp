using System;
using UnityEngine.Events;

namespace ProjectSP.Events
{
	[Serializable]
	public class IntUnityEvent : UnityEvent<int>
	{
	}
}