using ProjectSP.Items;
using UnityEngine;

namespace ProjectSP
{
	[CreateAssetMenu(menuName = "Items/PlayerConfig")]
	public class PlayerConfig : ScriptableObject
	{
		public Weapon weapon;
	}
}