using System.Collections.Generic;
using UnityEngine;

namespace ProjectSP
{
	public class PoolController : MonoBehaviour
	{
		[SerializeField] private List<GameObject> pooledObjects;
		[Range(1, 100)] [SerializeField] private int startPoolSize;
		[Range(1, 100)] [SerializeField] private int additionalPoolResize = 5;
		[SerializeField] private Transform root;

		private void Awake()
		{
			foreach (var obj in pooledObjects)
			{
				for (int i = 0; i < startPoolSize; i++)
				{
					PoolObject(obj);
				}
			}
		}

		public void PoolObjects(GameObject obj)
		{
			for (int i = 0; i < additionalPoolResize; i++)
			{
				PoolObject(obj);
			}
		}

		private void PoolObject(GameObject obj)
		{
			var enemy = Instantiate(obj, root);
			enemy.SetActive(false);
		}
	}
}