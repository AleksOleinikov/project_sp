using System.Collections.Generic;
using System.Linq;
using ProjectSP.Events;
using ProjectSP.PowerUps;
using UnityEngine;
using UnityEngine.AI;

namespace ProjectSP
{
	public class PowerUpManager : MonoBehaviour
	{
		[SerializeField] private List<PowerUpEntity> powerUps;
		[SerializeField] private Vector3Variable currentPlayerPos;
		[SerializeField] private float startDropChance = 25;
		[SerializeField] private float dropIncreasePerTry = 5;
		[SerializeField] private GameObjectGameEvent poolIsEmptyEvent;
		[SerializeField] private float dropRadius = 3;

		private float currentDropChance;

		public void OnGameStart()
		{
			Reset();
		}


		public void OnEnemyDeath()
		{
			if (!powerUps.Any(x => x.CanDrop)) return;
			var rnd = Random.Range(0, 100);
			if (rnd > currentDropChance)
			{
				currentDropChance += dropIncreasePerTry;
				return;
			}

			var possibleDrop = powerUps.Where(x => x.CanDrop).ToArray();
			var dropId = Random.Range(0, possibleDrop.Length - 1);
			DropPowerUp(possibleDrop[dropId]);
		}

		private void Reset()
		{
			foreach (var pu in powerUps)
			{
				pu.DropCount = 0;
			}

			currentDropChance = startDropChance;
		}

		private void DropPowerUp(PowerUpEntity pu)
		{
			if (pu.powerUpsSet.Items.Count <= 0)
			{
				poolIsEmptyEvent.Raise(pu.targetPrefab);
				return;
			}

			var go = pu.powerUpsSet.Items.LastOrDefault();
			if (go == null) return;
			go.transform.position = GetRandomPosition();
			go.gameObject.SetActive(true);
			pu.DropCount++;
		}

		private Vector3 GetRandomPosition()
		{
			Vector3 randomDirection = Random.insideUnitSphere * dropRadius;
			randomDirection += currentPlayerPos.Value;
			NavMesh.SamplePosition(randomDirection, out var hit, dropRadius, 1);
			return hit.position;
		}
	}
}