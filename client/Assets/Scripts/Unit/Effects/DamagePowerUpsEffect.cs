using ProjectSP.Variables;
using UnityEngine;

namespace ProjectSP.Effect
{
	public class DamagePowerUpsEffect : Effect, IDamageModifier
	{
		[SerializeField] private IntVariable currentDamagePUsCount;
		[SerializeField] private int maxCount;
		[SerializeField] private float damageBonusPerPU;

		private void Awake()
		{
			ResetPowerUps();
		}

		public float Multiplier()
		{
			return currentDamagePUsCount.Value * damageBonusPerPU;
		}

		private void ResetPowerUps()
		{
			currentDamagePUsCount.SetValue(0);
		}

		public void GetPowerUp(int count)
		{
			if (currentDamagePUsCount.Value >= maxCount) return;
			currentDamagePUsCount.SetValue(Mathf.Clamp(currentDamagePUsCount.Value + count, 0, maxCount));
		}
	}
}