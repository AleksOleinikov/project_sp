using ProjectSP.Events;
using UnityEngine;

namespace ProjectSP
{
	public class PowerUpController : MonoBehaviour
	{
		[SerializeField] private IntGameEvent powerUpCollectedEvent;
		[SerializeField] private int powerUpCount;
		[SerializeField] private float lifeTime = 15;
		[SerializeField] private GameObjectsSet powerUpsSet;
		[SerializeField] private Renderer renderer;

		private float activeteTime;

		private void OnEnable()
		{
			powerUpsSet.Remove(gameObject);
			activeteTime = Time.timeSinceLevelLoad;
			ResetColor();
		}

		private void OnDisable()
		{
			powerUpsSet.Add(gameObject);
		}

		float _timePassed;

		private void Update()
		{
			var lastTime = activeteTime + lifeTime - Time.timeSinceLevelLoad;
			if (lastTime < 5)
			{
				_timePassed += Time.deltaTime;
				var color = renderer.material.GetColor("_BaseColor");
				var targetAlpha = Mathf.Lerp(1, .5f, Mathf.PingPong(_timePassed, 1));
				color.a = targetAlpha;
				renderer.material.SetColor("_BaseColor", color);
			}

			if (lastTime > 0) return;
			Hide();
		}

		private void OnTriggerEnter(Collider other)
		{
			if (other.tag != "Player") return;
			powerUpCollectedEvent.Raise(powerUpCount);
			Hide();
		}

		private void ResetColor()
		{
			var color = renderer.material.GetColor("_BaseColor");
			color.a = 1;
			renderer.material.SetColor("_BaseColor", color);
		}

		private void Hide()
		{
			gameObject.SetActive(false);
		}
	}
}