using ProjectSP.Events;
using ProjectSP.Variables;
using UnityEngine;

namespace ProjectSP
{
	public class ProjectileController : MonoBehaviour
	{
		[SerializeField] private float moveSpeed;
		[SerializeField] private IntGameEvent dealToPlayerDamageEvent;
		[SerializeField] private IntReference damage;

		private void FixedUpdate()
		{
			MoveForward();
		}

		private void MoveForward()
		{
			transform.localPosition += transform.forward * moveSpeed;
		}

		private void OnTriggerEnter(Collider other)
		{
			switch (other.tag)
			{
				case "Player":
					DealDamage();
					break;
				case "Wall":
					Destroy(gameObject);
					break;
			}
		}


		private void DealDamage()
		{
			dealToPlayerDamageEvent.Raise(damage.Value);
			Destroy(gameObject);
		}
	}
}