using System;
using UnityEngine;

namespace ProjectSP.Variables
{
	[CreateAssetMenu(menuName = "Variables/IntVariable")]
	public class IntVariable : ScriptableObject
	{
		public int Value;
		public Action onValueChange;

		public void SetValue(int intValue)
		{
			Value = intValue;
			onValueChange?.Invoke();
		}

		public void SetValue(IntVariable intVariable)
		{
			Value = intVariable.Value;
			onValueChange?.Invoke();
		}

		public void ApplyChange(int intValue)
		{
			Value += intValue;
			onValueChange?.Invoke();
		}

		public void ApplyChange(IntVariable intVariable)
		{
			Value += intVariable.Value;
			onValueChange?.Invoke();
		}
	}
}