using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ProjectSP
{
	public abstract class BaseVirtualJoystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
	{
		public Camera uiCamera;

		[Header("Handle limit")] [Range(0f, 2f)] [SerializeField]
		protected float handleLimit = 1f;

		[Header("Dead zone")] [SerializeField] [Range(0f, 2f)]
		protected float zeroSensibilityRadius = 0.0f;

		[SerializeField] [Range(0f, 1.0f)] protected float minInputMagnitude = 0.0f;

		[Header("Sensibility")] [SerializeField]
		protected float minInputSnapSpeed = 10f;

		[SerializeField] protected float maxInputSnapSpeed = 100000000f;
		[SerializeField] private float snapSpeed;
		[Header("Tap")] [SerializeField] protected int tapFramesCount = 3;

		protected Vector2 inputVector = Vector2.zero;
		private bool enableUpdateInput = true;
		private WaitForEndOfFrame waitForEndOfFrame;
		private int beginFrame;
		public bool TouchUp { get; protected set; }
		public bool Tap { get; protected set; }
		public bool Draged { get; protected set; }

		[Header("Components")] [SerializeField]
		protected RectTransform background;

		[SerializeField] protected RectTransform handle;

		[Header("Input")] [SerializeField] protected string horizontalAxis = "Horizontal";
		[SerializeField] protected string verticalAxis = "Vertical";

		private Vector2 smoothedInputVector;

		public float Horizontal
		{
			get { return smoothedInputVector.x; }
		}

		public float Vertical
		{
			get { return smoothedInputVector.y; }
		}

		public Action OnPointerUpEvent;
		public Action OnPointerDownEvent;

		protected virtual void Awake()
		{
			snapSpeed = maxInputSnapSpeed;
		}

		public Vector2 InputVector
		{
			get { return smoothedInputVector; }
		}

		public void ForceStopDrag()
		{
			if (Draged) OnPointerUp(null);
			Update();
		}


		public virtual void OnDrag(PointerEventData eventData)
		{
			enableUpdateInput = false;
		}

		public virtual void OnPointerDown(PointerEventData eventData)
		{
			enableUpdateInput = false;
			beginFrame = Time.frameCount;
			Draged = true;
			OnPointerDownEvent?.Invoke();
		}

		public virtual void OnPointerUp(PointerEventData eventData)
		{
			enableUpdateInput = true;
			TouchUp = true;
			int currentFrame = Time.frameCount;
			Tap = (currentFrame - beginFrame <= tapFramesCount);
			Draged = false;
			OnPointerUpEvent?.Invoke();
		}

		public void ResetStick(bool hasTarget)
		{
			TouchUp = false;
			Tap = false;
			snapSpeed = hasTarget ? minInputSnapSpeed : maxInputSnapSpeed;
		}

		protected abstract void UpdateInput();

		protected void Update()
		{
			smoothedInputVector = Vector3.RotateTowards(smoothedInputVector, inputVector,
				Time.smoothDeltaTime * snapSpeed, Mathf.Infinity);

			if (enableUpdateInput)
			{
				UpdateInput();
			}
		}
	}
}