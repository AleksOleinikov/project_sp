using ProjectSP.Events;
using ProjectSP.Variables;
using UnityEngine;

namespace ProjectSP
{
	public class EnemyOnCloseRangeDamageDealer : MonoBehaviour
	{
		[SerializeField] private IntReference unitDamage;
		[SerializeField] private FloatReference damageDealRange;
		[SerializeField] private FloatReference damageCooldownTime;

		[SerializeField] private IntGameEvent dealToPlayerDamageEvent;
		[SerializeField] private Vector3Variable playerPosition;

		private float lastAttackTime;

		private bool InDamageRange =>
			damageDealRange.Value >= Vector3.Distance(playerPosition.Value, transform.position);

		private bool OnCooldown => Time.timeSinceLevelLoad < lastAttackTime + damageCooldownTime.Value;

		private void LateUpdate()
		{
			if (OnCooldown || !InDamageRange) return;
			DealDamageToPlayer();
			lastAttackTime = Time.timeSinceLevelLoad;
		}

		private void DealDamageToPlayer()
		{
			dealToPlayerDamageEvent.Raise(unitDamage.Value);
		}
	}
}