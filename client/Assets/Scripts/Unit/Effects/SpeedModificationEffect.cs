using UnityEngine;

namespace ProjectSP.Effect
{
	public class SpeedModificationEffect : Effect, ISpeedModifier
	{
		[SerializeField] private float modification = 0.5f;

		public float Multiplier()
		{
			return modification;
		}
	}
}