namespace ProjectSP.Effect
{
	public interface ISpeedModifier
	{
		float Multiplier();
	}
}