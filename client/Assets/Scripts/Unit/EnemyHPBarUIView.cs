using UnityEngine;
using UnityEngine.UI;

namespace ProjectSP.Unit
{
	public class EnemyHPBarUIView : MonoBehaviour
	{
		[SerializeField] private EnemyParameters parameters;
		[SerializeField] private Image fill;

		private void Awake()
		{
			parameters.onHealthChange += UpdateView;
			UpdateView();
		}

		private void UpdateView()
		{
			fill.fillAmount = parameters.CurrentHP / parameters.startHP.Value;
		}
	}
}