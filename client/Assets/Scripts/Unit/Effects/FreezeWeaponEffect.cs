using UnityEngine;

namespace ProjectSP.Effect
{
	public class FreezeWeaponEffect : Effect
	{
		[SerializeField] private SpeedModificationEffect freezeEffect;
		[SerializeField] private EnemiesSet currentEnemies;
		[Range(0, 1)] [SerializeField] private float procChance = .15f;
		[SerializeField] private float cooldownTime;

		private float lastProcTime;

		private PseudoRandomBoolean procNow;

		private void Awake()
		{
			procNow = new PseudoRandomBoolean(procChance);
			lastProcTime = -cooldownTime;
		}

		public void TryToProc()
		{
			if (lastProcTime + cooldownTime > Time.timeSinceLevelLoad) return;
			if (!procNow) return;
			lastProcTime = Time.timeSinceLevelLoad;
			foreach (var enemy in currentEnemies.Items)
			{
				enemy.EffectManager.CreateEffect(freezeEffect);
			}
		}
	}
}