using UnityEngine;

namespace ProjectSP
{
	[CreateAssetMenu(menuName = "Sets/EnemiesSet")]
	public class EnemiesSet : RuntimeSet<EnemyController>
	{
	}
}