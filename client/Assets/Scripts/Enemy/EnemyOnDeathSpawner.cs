using System.Linq;
using ProjectSP.Events;
using UnityEngine;

namespace ProjectSP
{
	public class EnemyOnDeathSpawner : MonoBehaviour
	{
		[SerializeField] private EnemiesSet currentEnemiesPool;
		[SerializeField] private int count = 2;
		[SerializeField] private GameObjectGameEvent poolIsEmptyEvent;
		[SerializeField] private GameObject targetEnemyPrefab;
		[SerializeField] private EnemyController controller;

		private void Awake()
		{
			controller.onEnemyDeath += OnDeathSpawn;
		}

		private void OnDeathSpawn()
		{
			for (int i = 0; i < count; i++)
			{
				Spawn(transform.position);
			}
		}

		private void Spawn(Vector3 position)
		{
			if (currentEnemiesPool.Items.Count <= 0)
			{
				poolIsEmptyEvent.Raise(targetEnemyPrefab);
				return;
			}

			var go = currentEnemiesPool.Items.LastOrDefault();
			if (go == null) return;
			go.transform.position = position;
			go.gameObject.SetActive(true);
		}
	}
}