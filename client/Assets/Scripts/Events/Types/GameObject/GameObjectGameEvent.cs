using UnityEngine;

namespace ProjectSP.Events
{
	[CreateAssetMenu(menuName = "Events/GameObjectGameEvent")]
	public class GameObjectGameEvent : BaseGameEvent<GameObject>
	{
		[SerializeField] private GameObject value;
		public override void Raise() => Raise(value);
	}
}