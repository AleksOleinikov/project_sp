using ProjectSP.Events;
using ProjectSP.Variables;
using UnityEngine;

namespace ProjectSP
{
	public class BallisticProjectileController : MonoBehaviour
	{
		[SerializeField] private AnimationCurve heightByDistance;
		[SerializeField] private float moveSpeed;
		[SerializeField] private IntGameEvent dealToPlayerDamageEvent;
		[SerializeField] private IntReference damage;
		[SerializeField] private Vector3Variable currentPlayerPos;
		[SerializeField] private float damageDealDistance;
		[SerializeField] private Transform projectileRoot;
		[SerializeField] private GameObject damageZone;

		private float fractionOfJourney;
		private Vector3 targetPosition;

		private float startTime;

		// Total distance between the markers.
		private float journeyLength;

		private void OnEnable()
		{
			Init();
		}

		private void FixedUpdate()
		{
			MoveForward();
			SetHeightByDistance();
			if (fractionOfJourney < 1) return;
			TryDealDamage();
		}

		private void MoveForward()
		{
			float distCovered = (Time.timeSinceLevelLoad - startTime) * moveSpeed;

			fractionOfJourney = distCovered / journeyLength;

			projectileRoot.position = Vector3.Lerp(transform.position, targetPosition, fractionOfJourney);
		}

		private void SetHeightByDistance()
		{
			var height = heightByDistance.Evaluate(fractionOfJourney);
			projectileRoot.position = new Vector3(projectileRoot.position.x, height, projectileRoot.position.z);
		}

		private void TryDealDamage()
		{
			damageZone.SetActive(false);
			projectileRoot.gameObject.SetActive(false);
			if (Vector3.Distance(currentPlayerPos.Value, projectileRoot.position) <= damageDealDistance)
			{
				dealToPlayerDamageEvent.Raise(damage.Value);
			}

			Destroy(gameObject);
		}

		private void Init()
		{
			targetPosition = currentPlayerPos.Value;
			projectileRoot.localPosition = Vector3.zero;
			SetHeightByDistance();
			SetZoneScale();
			damageZone.transform.position = currentPlayerPos.Value;
			damageZone.SetActive(true);
			projectileRoot.gameObject.SetActive(true);

			startTime = Time.timeSinceLevelLoad;
			journeyLength = Vector3.Distance(projectileRoot.position, targetPosition);
		}

		private void SetZoneScale()
		{
			damageZone.transform.localScale = new Vector3(damageDealDistance, damageDealDistance, damageDealDistance);
		}
	}
}