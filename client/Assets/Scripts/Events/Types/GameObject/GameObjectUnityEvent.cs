using System;
using UnityEngine;
using UnityEngine.Events;

namespace ProjectSP.Events
{
	[Serializable]
	public class GameObjectUnityEvent : UnityEvent<GameObject>
	{
	}
}