using System.Collections;
using UnityEngine;

namespace ProjectSP
{
	public class AnimateSpriteSheet : MonoBehaviour
	{
		[SerializeField] private int columns = 8;
		[SerializeField] private int rows = 8;
		[SerializeField] private float framesPerSecond = 32f;

		private Renderer _renderer;
		private int index;

		void Awake()
		{
			_renderer = this.GetComponent<Renderer>();
		}

		void Start()
		{
			StartCoroutine(UpdateTiling());
			Vector2 size = new Vector2(1f / columns, 1f / rows);
			_renderer.material.SetTextureScale("_BaseMap", size);
		}

		IEnumerator UpdateTiling()
		{
			float x;
			float y;
			Vector2 offset;
			while (true)
			{
				index++;
				if (index >= rows * columns)
					index = 0;

				x = (float) index / columns - (index / columns);
				y = 1 - (index / columns) / (float) rows;
				offset = new Vector2(x, y);
				_renderer.material.SetTextureOffset("_BaseMap", offset);

				yield return new WaitForSeconds(1f / framesPerSecond);
			}
		}
	}
}