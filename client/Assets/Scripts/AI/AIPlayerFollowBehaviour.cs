using UnityEngine;
using UnityEngine.AI;

namespace ProjectSP
{
	public class AIPlayerFollowBehaviour : MonoBehaviour
	{
		[SerializeField] private NavMeshAgent navAgent;
		[SerializeField] private Vector3Variable currentPlayerPos;

		private EnemyParameters parameters;

		private void Awake()
		{
			parameters = GetComponent<EnemyParameters>();
		}

		private void LateUpdate()
		{
			FollowPlayer();
		}

		private void FollowPlayer()
		{
			navAgent.speed = parameters.CurrentMoveSpeed;
			navAgent.SetDestination(currentPlayerPos.Value);
		}
	}
}