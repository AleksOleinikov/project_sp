using UnityEngine;

namespace ProjectSP.Effect
{
	public class PlayerSlowOnEnemiesInRangeEffect : Effect, ISpeedModifier
	{
		[SerializeField] private FloatReference slowMultiplier;
		[SerializeField] private EnemiesSet targetEnemies;

		public float Multiplier()
		{
			return targetEnemies.Items.Count > 0 ? slowMultiplier.Value : 1;
		}
	}
}