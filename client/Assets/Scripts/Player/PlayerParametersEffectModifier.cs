using ProjectSP.Effect;
using UnityEngine;

namespace ProjectSP
{
	public class PlayerParametersEffectModifier : MonoBehaviour
	{
		[SerializeField] private EffectManager effectManager;
		[SerializeField] private FloatReference playerBaseMovementSpeed;
		[SerializeField] private FloatVariable playerCurrentMovementSpeed;

		private void Update()
		{
			RecalculateSpeed();
		}

		private void RecalculateSpeed()
		{
			var currentSpeed = playerBaseMovementSpeed.Value;
			foreach (var effect in effectManager.currentEffects)
			{
				if (!(effect is ISpeedModifier speedEffect)) continue;
				currentSpeed *= speedEffect.Multiplier();
			}

			playerCurrentMovementSpeed.SetValue(currentSpeed);
		}
	}
}