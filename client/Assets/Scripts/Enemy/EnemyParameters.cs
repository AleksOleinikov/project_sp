using System;
using ProjectSP.Variables;
using UnityEngine;

namespace ProjectSP
{
	public class EnemyParameters : MonoBehaviour
	{
		public IntReference startHP;
		public FloatReference baseMoveSpeed;

		private float currentHp;

		public float CurrentHP
		{
			get => currentHp;
			set
			{
				currentHp = value;
				onHealthChange?.Invoke();
			}
		}

		public float CurrentMoveSpeed { get; set; }

		public Action onHealthChange;

		public void Reset()
		{
			CurrentHP = startHP.Value;
		}
	}
}