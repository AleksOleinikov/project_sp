using UnityEngine;

namespace ProjectSP.Events
{
	[CreateAssetMenu(menuName = "Events/IntGameEvent")]
	public class IntGameEvent : BaseGameEvent<int>
	{
		[SerializeField] private int value;
		public override void Raise() => Raise(value);
	}
}