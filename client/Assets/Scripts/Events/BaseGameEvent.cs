using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ProjectSP.Events
{
	public abstract class BaseGameEvent<T> : ScriptableObject, IGameEvent
	{
		private readonly List<IGameEventListener<T>> eventListeners = new List<IGameEventListener<T>>();

		public abstract void Raise();

		public void Raise(T item)
		{
			for (int i = eventListeners.Count - 1; i >= 0; i--)
			{
				eventListeners[i].OnEventRaised(item);
			}
		}

		public void RegisterListener(IGameEventListener<T> listener)
		{
			if (!eventListeners.Contains(listener))
			{
				eventListeners.Add(listener);
			}
		}

		public void UnregisterListener(IGameEventListener<T> listener)
		{
			if (eventListeners.Contains(listener))
			{
				eventListeners.Remove(listener);
			}
		}
	}
}