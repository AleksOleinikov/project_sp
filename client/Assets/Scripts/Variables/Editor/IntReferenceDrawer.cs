using ProjectSP.Variables;
using UnityEditor;

namespace Variables.Editor
{
	[CustomPropertyDrawer(typeof(IntReference))]
	public class IntReferenceDrawer : VariableReferenceDrawer
	{
	}
}