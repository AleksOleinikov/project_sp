using UnityEngine;

namespace ProjectSP.Events
{
	public class BattleController : MonoBehaviour
	{
		[SerializeField] private VoidGameEvent battleStartEvent;
		[SerializeField] private VoidGameEvent battleVictoryEvent;
		[SerializeField] private VoidGameEvent battleLostEvent;

		[SerializeField] private GameObjectsSet activeSpawners;
		[SerializeField] private EnemiesSet activeEnemies;

		private void Start()
		{
			battleStartEvent.Raise();
		}

		public void OnPlayerDeath()
		{
			battleLostEvent.Raise();
		}

		public void OnEnemyDeath()
		{
			if (activeSpawners.Items.Count > 0 || activeEnemies.Items.Count > 0) return;
			battleVictoryEvent.Raise();
		}
	}
}