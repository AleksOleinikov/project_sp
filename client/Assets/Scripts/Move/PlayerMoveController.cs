using UnityEngine;
using UnityEngine.AI;

namespace ProjectSP
{
	public class PlayerMoveController : MonoBehaviour
	{
		[SerializeField] private Vector3Variable moveVector;
		[SerializeField] private Vector3Variable currentPlayerPos;
		[SerializeField] private FloatReference playerMoveSpeed;
		[SerializeField] private NavMeshAgent navAgent;

		private void FixedUpdate()
		{
			Move();
			currentPlayerPos.SetValue(transform.position);
		}

		private void Move()
		{
			if (moveVector.Value.magnitude <= 0) return;
			var movePosition = moveVector.Value * (Time.deltaTime * playerMoveSpeed.Value);
			navAgent.Move(movePosition);
		}
	}
}