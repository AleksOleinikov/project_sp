using UnityEngine;

namespace ProjectSP
{
	[CreateAssetMenu(menuName = "Sets/GameObjectsSet")]
	public class GameObjectsSet : RuntimeSet<GameObject>
	{
	}
}