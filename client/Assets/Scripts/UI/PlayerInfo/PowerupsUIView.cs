using System.Collections.Generic;
using UnityEngine;

namespace ProjectSP
{
	public class PowerupsUIView : MonoBehaviour
	{
		[SerializeField] private List<GameObject> elements;

		public void UpdateView(int activeCount)
		{
			for (int i = 0; i < elements.Count; i++)
			{
				elements[i].SetActive(i < activeCount);
			}
		}
	}
}