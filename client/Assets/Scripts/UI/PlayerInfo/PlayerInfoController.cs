using ProjectSP.Variables;
using UnityEngine;
using UnityEngine.UI;

namespace ProjectSP
{
	public class PlayerInfoController : MonoBehaviour
	{
		[Header("HP")] [SerializeField] private IntVariable startPlayerHP;
		[SerializeField] private IntVariable currentPlayerHP;
		[SerializeField] private Image hpBarFill;

		[Header("Potions")] [SerializeField] private IntVariable currentPotionsCount;
		[SerializeField] private PowerupsUIView potionsView;

		[Header("Damage")] [SerializeField] private IntVariable currentDamageCount;
		[SerializeField] private PowerupsUIView damageView;


		private void Awake()
		{
			currentPlayerHP.onValueChange = UpdateHealthBar;
			currentPotionsCount.onValueChange = UpdatePotionsCount;
			currentDamageCount.onValueChange = UpdateDamgeCount;
			UpdateView();
		}

		private void UpdateView()
		{
			UpdateHealthBar();
			UpdatePotionsCount();
			UpdateDamgeCount();
		}

		private void UpdateHealthBar()
		{
			hpBarFill.fillAmount = (float) currentPlayerHP.Value / startPlayerHP.Value;
		}

		private void UpdatePotionsCount()
		{
			potionsView.UpdateView(currentPotionsCount.Value);
		}

		private void UpdateDamgeCount()
		{
			damageView.UpdateView(currentDamageCount.Value);
		}
	}
}