using System;
using UnityEngine;

namespace ProjectSP.PowerUps
{
	[Serializable]
	public class PowerUpEntity
	{
		public GameObjectsSet powerUpsSet;
		public GameObject targetPrefab;
		public int maxPowerUpsCount;
		public int DropCount { get; set; }

		public bool CanDrop => maxPowerUpsCount > DropCount;
	}
}