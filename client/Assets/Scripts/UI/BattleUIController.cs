using UnityEngine;

namespace ProjectSP.UI
{
	public class BattleUIController : MonoBehaviour
	{
		[SerializeField] private GameObject battleRoot;
		[SerializeField] private GameObject battleLostView;
		[SerializeField] private GameObject battleVictoryView;

		private void Awake()
		{
			battleRoot.SetActive(false);
			battleLostView.SetActive(false);
			battleVictoryView.SetActive(false);
		}

		public void OnBattleStart()
		{
			battleRoot.SetActive(true);
		}

		public void OnBattleEnd(bool victory)
		{
			battleRoot.SetActive(false);
			battleLostView.SetActive(!victory);
			battleVictoryView.SetActive(victory);
		}
	}
}