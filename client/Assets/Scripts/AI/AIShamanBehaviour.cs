using UnityEngine;
using UnityEngine.AI;

namespace ProjectSP
{
	public class AIShamanBehaviour : MonoBehaviour
	{
		[SerializeField] private NavMeshAgent navAgent;
		[SerializeField] private Vector3Variable currentPlayerPos;
		[SerializeField] private float walkRadius = 3;

		[Header("AttackParameters")] [SerializeField]
		private GameObject projectile;

		[SerializeField] private float attackPeriod;
		[SerializeField] private Transform shootPoint;

		private EnemyParameters parameters;

		private float lastAttackTime;
		private Vector3 targetPos;

		private void Awake()
		{
			parameters = GetComponent<EnemyParameters>();
			targetPos = GetRandomPosition();
		}

		private void LateUpdate()
		{
			if (lastAttackTime + attackPeriod > Time.timeSinceLevelLoad)
			{
				MoveToPoint();
			}
			else
			{
				Attack();
			}
		}

		private void MoveToPoint()
		{
			navAgent.speed = parameters.CurrentMoveSpeed;
			navAgent.SetDestination(targetPos);
		}

		private Vector3 GetRandomPosition()
		{
			Vector3 randomDirection = Random.insideUnitSphere * walkRadius;
			randomDirection += transform.position;
			NavMesh.SamplePosition(randomDirection, out var hit, walkRadius, 1);
			return hit.position;
		}

		private void Attack()
		{
			lastAttackTime = Time.timeSinceLevelLoad;
			var bullet = Instantiate(projectile, transform.parent);
			bullet.SetActive(false);
			bullet.transform.position = shootPoint.position;
			bullet.transform.LookAt(new Vector3(currentPlayerPos.Value.x, shootPoint.position.y,
				currentPlayerPos.Value.z));
			bullet.SetActive(true);
			targetPos = GetRandomPosition();
		}
	}
}