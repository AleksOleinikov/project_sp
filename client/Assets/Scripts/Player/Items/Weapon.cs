using System.Collections.Generic;
using ProjectSP.Variables;
using UnityEngine;

namespace ProjectSP.Items
{
	[CreateAssetMenu(menuName = "Items/Weapon")]
	public class Weapon : ScriptableObject
	{
		public Sprite weaponImage;
		public IntReference damage;

		public FloatReference attackTimePeriod;

		public FloatReference attackRange;

		public FloatReference slowMultiplier;

		public List<Effect.Effect> effects;
		//TODO: 3D, effects
	}
}