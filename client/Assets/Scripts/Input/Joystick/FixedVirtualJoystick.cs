using UnityEngine;
using UnityEngine.EventSystems;

namespace ProjectSP
{
	public class FixedVirtualJoystick : BaseVirtualJoystick
	{
		private bool canTouchUp;

		protected float HalfSizeBackground
		{
			get { return background.sizeDelta.x * 0.5f; }
		}

		private float LimitedHalfSizeBackground
		{
			get { return HalfSizeBackground * handleLimit; }
		}


		private void UpdateJoystick(Vector2 direction)
		{
			Vector2 clampedDir = ClampDirection(direction);
			float unshiftedMagn = clampedDir.magnitude;
			float shiftedMagn = Mathf.Min((unshiftedMagn - zeroSensibilityRadius) / (1.0f - zeroSensibilityRadius),
				1.0f);
			shiftedMagn = shiftedMagn < 0 ? 0 : Mathf.Lerp(minInputMagnitude, 1.0f, shiftedMagn);
			inputVector = clampedDir.normalized * shiftedMagn;
			Vector2 anchoredPos = clampedDir * LimitedHalfSizeBackground;
			handle.anchoredPosition = anchoredPos;
		}

		public override void OnDrag(PointerEventData eventData)
		{
			var origin = RectTransformUtility.WorldToScreenPoint(uiCamera, background.position);
			Vector2 unfilteredDirection = eventData.position - origin;

			UpdateJoystick(unfilteredDirection);
			base.OnDrag(eventData);
		}

		private Vector2 ClampDirection(Vector2 inputDir)
		{
			return (inputDir.magnitude > LimitedHalfSizeBackground)
				? inputDir.normalized
				: inputDir / LimitedHalfSizeBackground;
		}

		public override void OnPointerDown(PointerEventData eventData)
		{
			canTouchUp = false;
			base.OnPointerDown(eventData);
			OnDrag(eventData);
		}

		public override void OnPointerUp(PointerEventData eventData)
		{
			canTouchUp = false;
			inputVector = Vector2.zero;
			handle.anchoredPosition = Vector2.zero;
			base.OnPointerUp(eventData);
		}

		protected override void UpdateInput()
		{
			float x = Input.GetAxis(horizontalAxis) * HalfSizeBackground;
			float y = Input.GetAxis(verticalAxis) * HalfSizeBackground;

			Vector2 direction = new Vector2(
				x,
				y
			);

			if (Mathf.Max(Mathf.Abs(x), Mathf.Abs(y)) <= 0.001f)
			{
				if (canTouchUp)
				{
					TouchUp = true;
					canTouchUp = false;
				}
			}
			else
			{
				canTouchUp = true;
			}

			UpdateJoystick(direction);
		}
	}
}