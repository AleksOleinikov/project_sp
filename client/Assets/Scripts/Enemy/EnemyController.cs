using System;
using ProjectSP.Effect;
using ProjectSP.Events;
using UnityEngine;

namespace ProjectSP
{
	[RequireComponent(typeof(EnemyParameters))]
	public class EnemyController : MonoBehaviour
	{
		[SerializeField] private EnemiesSet allEnemiesSet;
		[SerializeField] private EnemiesSet activeEnemiesSet;
		[SerializeField] private EnemiesSet inactiveEnemiesSet;
		[SerializeField] private EffectManager effectManager;
		[SerializeField] private VoidGameEvent onDeathEvent;

		public EnemyParameters Parameters;
		public EffectManager EffectManager => effectManager;
		public Action onEnemyDeath;

		private void Awake()
		{
			Parameters = GetComponent<EnemyParameters>();
			if (allEnemiesSet != null) allEnemiesSet.Add(this);
			else Debug.LogError("AllEnemiesSet is null for " + this.name);
		}

		private void OnEnable()
		{
			if (activeEnemiesSet != null) activeEnemiesSet.Add(this);
			else Debug.LogError("ActiveEnemiesSet is null for " + this.name);
			if (inactiveEnemiesSet != null) inactiveEnemiesSet.Remove(this);
			Reset();
		}

		private void OnDisable()
		{
			if (activeEnemiesSet != null) activeEnemiesSet.Remove(this);
			else Debug.LogError("ActiveEnemiesSet is null for " + this.name);
			if (inactiveEnemiesSet != null) inactiveEnemiesSet.Add(this);
		}

		private void OnDestroy()
		{
			if (allEnemiesSet != null) allEnemiesSet.Remove(this);
			else Debug.LogError("AllEnemiesSet is null for " + this.name);
		}

		private void Update()
		{
			RecalculateSpeed();
		}

		private void RecalculateSpeed()
		{
			var currentSpeed = Parameters.baseMoveSpeed.Value;
			foreach (var effect in effectManager.currentEffects)
			{
				if (effect == null) continue;
				if (!(effect is ISpeedModifier speedEffect)) continue;
				currentSpeed *= speedEffect.Multiplier();
			}

			Parameters.CurrentMoveSpeed = currentSpeed;
		}

		public void Reset()
		{
			Parameters.CurrentHP = Parameters.startHP.Value;
		}

		public void DealDamage(float amount)
		{
			Parameters.CurrentHP -= amount;
			if (Parameters.CurrentHP <= 0)
			{
				Dead();
			}
		}

		private void Dead()
		{
			gameObject.SetActive(false);
			onEnemyDeath?.Invoke();
			onDeathEvent.Raise();
		}

		public void Heal(int amount)
		{
			Parameters.CurrentHP += amount;
		}
	}
}