using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectSP.Effect
{
	public class ChainLightningEffect : Effect
	{
		[SerializeField] private EnemiesSet currentEnemies;
		[Range(0, 1)] [SerializeField] private float procChance = .15f;
		[SerializeField] private int maxBounces = 5;
		[SerializeField] private float lightningDamage = 5;
		[SerializeField] private float bounceDelay = .5f;
		[SerializeField] private ChainLightningVisualEffect prefab;
		[SerializeField] private float cooldownTime;

		private float lastProcTime;


		private PseudoRandomBoolean procNow;
		private WaitForSeconds waitBounce;

		private void Awake()
		{
			procNow = new PseudoRandomBoolean(procChance);
			waitBounce = new WaitForSeconds(bounceDelay);
			lastProcTime = -cooldownTime;
		}

		public void TryToProc()
		{
			if (lastProcTime + cooldownTime > Time.timeSinceLevelLoad) return;
			if (!procNow) return;
			lastProcTime = Time.timeSinceLevelLoad;
			StartCoroutine(LightningDamageEffect());
		}

		IEnumerator LightningDamageEffect()
		{
			var strike = Instantiate(prefab);
			strike.gameObject.SetActive(false);
			var enemies = new List<EnemyController>(currentEnemies.Items);
			var prevPos = transform.position;
			for (int i = 0; i < maxBounces; i++)
			{
				if (enemies.Count <= 0) break;
				var enemy = enemies[Random.Range(0, enemies.Count - 1)];
				enemies.Remove(enemy);
				strike.ShowEffect(prevPos, enemy.transform.position);
				strike.gameObject.SetActive(true);
				prevPos = enemy.transform.position;
				yield return waitBounce;
				enemy.DealDamage(lightningDamage);
			}

			Destroy(strike.gameObject);
		}
	}
}