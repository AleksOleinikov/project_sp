﻿using UnityEditor;
using UnityEngine;
using Variables.Editor;

[CustomPropertyDrawer(typeof(FloatReference))]
public class FloatReferenceDrawer : VariableReferenceDrawer
{
}