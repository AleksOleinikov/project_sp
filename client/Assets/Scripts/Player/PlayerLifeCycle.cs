using ProjectSP.Events;
using ProjectSP.Variables;
using UnityEngine;

namespace ProjectSP
{
	public class PlayerLifeCycle : MonoBehaviour
	{
		[SerializeField] private VoidGameEvent playerDeadEvent;
		[SerializeField] private IntReference startHP;
		[SerializeField] private IntVariable currentHP;
		[Header("Potions")] [SerializeField] private IntVariable currentPotionsCount;
		[SerializeField] private int startPotionsCount = 2;
		[SerializeField] private int maxPotionsCount = 4;


		private void Awake()
		{
			Reset();
		}

		public void Reset()
		{
			currentHP?.SetValue(startHP.Value);
			currentPotionsCount.SetValue(startPotionsCount);
		}

		public void DealDamage(int amount)
		{
			currentHP?.ApplyChange(-amount);
			if (currentHP.Value > 0) return;
			if (currentPotionsCount.Value <= 0)
			{
				playerDeadEvent.Raise();
			}
			else
			{
				currentPotionsCount.ApplyChange(-1);
				currentHP.SetValue(startHP.Value);
			}
		}

		public void Heal(int amount)
		{
			currentHP?.ApplyChange(amount);
		}

		public void GetPotion(int count)
		{
			if (currentPotionsCount.Value >= maxPotionsCount) return;
			currentPotionsCount.SetValue(Mathf.Clamp(currentPotionsCount.Value + count, 0, maxPotionsCount));
		}
	}
}