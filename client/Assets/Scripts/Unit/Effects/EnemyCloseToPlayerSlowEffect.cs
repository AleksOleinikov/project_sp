using UnityEngine;

namespace ProjectSP.Effect
{
	public class EnemyCloseToPlayerSlowEffect : Effect, ISpeedModifier
	{
		[SerializeField] private PlayerConfig currentPlayerConfig;
		[SerializeField] private Vector3Variable playerPos;

		public float Multiplier()
		{
			var distanceToPlayer = Vector3.Distance(playerPos.Value, transform.position);
			var weapon = currentPlayerConfig.weapon;
			return distanceToPlayer <= weapon.attackRange.Value ? weapon.slowMultiplier.Value : 1;
		}
	}
}