using UnityEngine;

namespace ProjectSP
{
	public class PlayerEnemiesTargetController : MonoBehaviour
	{
		[SerializeField] private EnemiesSet activeEnemiesSet;
		[SerializeField] private EnemiesSet targetEnemies;
		[SerializeField] private Vector3Variable currentPlayerPos;
		[SerializeField] private PlayerConfig config;

		public void Update()
		{
			targetEnemies.Clear();
			var playerWeapon = config.weapon;
			foreach (var enemy in activeEnemiesSet.Items)
			{
				var distanceToPlayer = Vector3.Distance(enemy.transform.position, currentPlayerPos.Value);
				if (distanceToPlayer > playerWeapon.attackRange.Value) continue;
				targetEnemies.Add(enemy);
			}
		}
	}
}