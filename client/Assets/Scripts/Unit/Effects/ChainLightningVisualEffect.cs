using UnityEngine;

namespace ProjectSP.Effect
{
	public class ChainLightningVisualEffect : MonoBehaviour
	{
		[SerializeField] private LineRenderer line;

		public void ShowEffect(Vector3 origin, Vector3 target)
		{
			line.SetPositions(new[] {origin, target});
		}
	}
}