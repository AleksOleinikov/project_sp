using System.Collections.Generic;
using UnityEngine;

namespace ProjectSP.Effect
{
	public class EffectManager : MonoBehaviour
	{
		public List<Effect> baseEffects = new List<Effect>();
		public List<Effect> currentEffects = new List<Effect>();

		private void Awake()
		{
			foreach (var effect in baseEffects)
			{
				Add(effect);
			}
		}

		public void Add(Effect effect)
		{
			currentEffects.Add(effect);
			effect.SetEffectManager(this);
		}

		public void CreateEffect(Effect effect)
		{
			var obj = Instantiate(effect, transform);
			Add(obj);
		}

		public void Remove(Effect effect)
		{
			if (currentEffects.Contains(effect))
			{
				currentEffects.Remove(effect);
			}
		}
	}
}