using System.Linq;
using ProjectSP.Events;
using UnityEngine;

namespace ProjectSP
{
	public class EnemiesSpawner : MonoBehaviour
	{
		[SerializeField] private EnemiesSet currentEnemiesPool;
		[SerializeField] private GameObjectsSet activeSpawners;
		[SerializeField] private VoidGameEvent spawnerFinishedEvent;
		[SerializeField] private GameObjectGameEvent poolIsEmptyEvent;
		[SerializeField] private GameObject targetEnemyPrefab;

		[Header("Spawn Parameters")] [SerializeField]
		private float firstSpawnDelay;

		[SerializeField] private float spawnDelay;
		[SerializeField] private float enemiesCount;

		private int spawnedCount;
		private float nextSpawnTime;
		private bool spawnerActive;

		public void OnStartGame()
		{
			spawnerActive = true;
			spawnedCount = 0;
			activeSpawners.Add(gameObject);
			if (firstSpawnDelay <= 0)
			{
				SpawnEnemy();
			}
			else
			{
				nextSpawnTime = Time.timeSinceLevelLoad + firstSpawnDelay;
			}
		}

		public void OnEndGame()
		{
			spawnerActive = false;
		}

		private void Update()
		{
			if (!spawnerActive) return;
			if (nextSpawnTime > Time.timeSinceLevelLoad) return;
			if (spawnedCount >= enemiesCount) return;
			SpawnEnemy();
		}


		private void SpawnEnemy()
		{
			if (currentEnemiesPool.Items.Count <= 0)
			{
				poolIsEmptyEvent.Raise(targetEnemyPrefab);
				return;
			}

			var go = currentEnemiesPool.Items.LastOrDefault();
			if (go == null) return;
			go.transform.position = transform.position;
			go.gameObject.SetActive(true);
			spawnedCount++;
			nextSpawnTime = Time.timeSinceLevelLoad + spawnDelay;
			if (spawnedCount < enemiesCount) return;
			activeSpawners.Remove(gameObject);
			spawnerFinishedEvent.Raise();
		}
	}
}