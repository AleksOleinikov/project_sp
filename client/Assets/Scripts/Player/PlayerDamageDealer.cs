using ProjectSP.Effect;
using ProjectSP.Events;
using UnityEngine;

namespace ProjectSP
{
	public class PlayerDamageDealer : MonoBehaviour
	{
		[SerializeField] private PlayerConfig currentPlayerConfig;
		[SerializeField] private EnemiesSet targetEnemies;
		[SerializeField] private EffectManager effectManager;
		[SerializeField] private VoidGameEvent dealDamageEvent;

		private float lastDamageTime;

		private void Update()
		{
			if (targetEnemies.Items.Count <= 0 || Time.timeSinceLevelLoad <
			    lastDamageTime + currentPlayerConfig.weapon.attackTimePeriod.Value) return;
			DealDamage();
		}

		private void DealDamage()
		{
			lastDamageTime = Time.timeSinceLevelLoad;
			dealDamageEvent.Raise();
			var damage = GetCurrentDamage();
			foreach (var enemy in targetEnemies.Items)
			{
				enemy.DealDamage(damage);
			}
		}

		private float GetCurrentDamage()
		{
			var multiplier = 1f;
			foreach (var effect in effectManager.currentEffects)
			{
				if (!(effect is IDamageModifier damageEffect)) continue;
				multiplier += damageEffect.Multiplier();
			}

			return currentPlayerConfig.weapon.damage * multiplier;
		}
	}
}