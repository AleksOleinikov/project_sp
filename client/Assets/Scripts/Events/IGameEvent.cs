namespace ProjectSP.Events
{
	public interface IGameEvent
	{
		void Raise();
	}
}