namespace ProjectSP.Effect
{
	public interface IDamageModifier
	{
		float Multiplier();
	}
}